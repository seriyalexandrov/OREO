package com.oreo.collector;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

// Import necessary libraries
@Component
public class PackageCollector {
    /**
     * HERE YOU NEED TO WRITE A PATH TO MONGODB
     */
    private static final Logger log = LoggerFactory.getLogger(PackageCollector.class.getName());

    @Value("${mongo.path}")
    private String path;

    @Value("${mongo.host}")
    private String host;

    @Value("${mongo.port}")
    private Integer port;

    @Value("${mongo.db}")
    private String dbName;

    @Value("${mongo.collection}")
    private String collectionName;

    private MongoClient mongo;
    private MongoDatabase database;
    private MongoCollection<Document> collection;

    public MongoCollection<Document> getCollection() {
        return collection;
    }

    @PostConstruct
    void init() {
        System.setProperty("DEBUG.MONGO", "true");
        if (!checkServer()) {
            upServer();
        } else {
            log.info("---------------------Server-is-Up---------------------");
        }
        mongo = new MongoClient("localhost", 27017);
        chooseDB();
        chooseCollection();
    }

    /**
     * Trying to set mongo-Server up
     * Attention: do not forget check the path to your server
     * If server doesn't open, check is there folder (YourDisk:\base\db)
     *
     * @return true if succeed, false if doesn't
     */
    private boolean upServer() {
        try {
            Process process = new ProcessBuilder(path + "mongod.exe").start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            //TODO find common feature showing that server is up and how to know that server is not OK
            log.info("----------------Try-to-raise-the-Server---------------");

            while ((line = br.readLine()) != null) {
                System.out.print(".");
                if (line.contains("[ftdc]") && line.contains("OK")) {
                    log.info("---------------------Server-is-Up---------------------");
                    break;
                }
                if (line.contains("now exiting")) {
                    log.info("--------------------Server-is-Down--------------------");
                    return false;
                }
            }
            return true;
        } catch (IOException ignore) {
            log.error("", ignore);
        }
        return false;
    }

    /**
     * Check is mongo-Server working
     *
     * @return true if it's up, false if it's down
     */
    private boolean checkServer() {
        String line;
        try {
            Process process = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");

            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while ((line = input.readLine()) != null) {
                if (line.contains("mongod.exe"))
                    return true;
            }
            input.close();
            return false;
        } catch (IOException ignore) {
            log.error("", ignore);
        }
        return false;
    }

    /**
     * Choose a database for working with it
     */
    public int chooseDB() {
        //credential = MongoCredential.createCredential("admin", dbName, "password".toCharArray());
        for (String db : mongo.listDatabaseNames()) {
            if (dbName.equals(dbName)) {
                database = mongo.getDatabase(dbName);
                return 0;
            }
        }
        database = mongo.getDatabase(dbName);
        return 1;
    }

    /**
     * Choose a collection to write down in it
     */
    public int chooseCollection() {

        if (database == null)
            return -1;
        for (String col : database.listCollectionNames()) {
            if (collectionName.equals(col)) {
                collection = database.getCollection(collectionName);
                return 0;
            }
        }
        database.createCollection(collectionName);
        collection = database.getCollection(collectionName);
        return 1;
    }

    /**
     * Write data of package in JSON into database
     *
     * @param obj JSON object with data of package
     * @return -1 if database or collection wasn't chosen, 0 if succeed
     */
    public int writeDoc(String obj) {
        if (database == null || collection == null)
            return -1;
        collection.insertOne(Document.parse(obj));
        return 0;
    }

    /**
     * Find all suitable documents in collection, if entry is empty return full collection
     *
     * @param entry pairs of parameters [key, value, key, value, ...]
     * @return Set's Iterator of suitable documents
     */
    public FindIterable<Document> readDocs(String[] entry) {
        if (database == null || collection == null || (entry.length & 1) == 1)
            return null;
        Document doc = new Document();
        if (entry.length == 0)
            return collection.find();
        for (int i = 0; i < entry.length / 2; i++) {
            doc.put(entry[2 * i], entry[2 * i + 1]);
        }
        return collection.find(doc);
    }

    /**
     * Find all suitable documents in collection, if obj is empty return full collection
     *
     * @param obj JSON object with necessary research parameters
     * @return Set's Iterator of suitable documents
     */
    public FindIterable<Document> readDocs(String obj) {
        if (database == null || collection == null || obj == null)
            return null;
        return collection.find(Document.parse(obj));
    }
}
