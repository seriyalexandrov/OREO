package com.oreo.database.model.link;

import javax.persistence.*;
import java.io.Serializable;

@Entity //аннотация, регистрирующая класс как сущность БД
@Table(name = "links") //связываем с конкретной таблицей по названию
public class Link implements Serializable {

    @Id //указывает на то , что следующее поля является ID и будет использоваться для поиска по умолчанию
    @Column(name = "id") //название колонки в таблице
    @GeneratedValue(strategy = GenerationType.IDENTITY) //автоматическое заполнение id
    private Long id; //навзвание поля класса

    @Column(name = "name")
    private String name;

    @Column(name = "source")
    private String source;

    @Column(name = "destination")
    private String destination;

    @Column(name = "utilization")
    private Long utilization;

    protected Link() {
    }

    public Link(String name, String source, String destination) {
        this.name = name;
        this.source = source;
        this.destination = destination;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return this.destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

}
