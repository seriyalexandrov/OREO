package com.oreo.database.model.port;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ports")
public class Port implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "ip")
    private String ip;


    protected Port() {
    }

    public Port(String ip) {
        this.ip = ip;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


}
