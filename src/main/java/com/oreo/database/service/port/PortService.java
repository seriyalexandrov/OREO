package com.oreo.database.service.port;

import com.oreo.database.model.port.Port;

import java.util.List;

public interface PortService {
    public void savePort(Port port);

    public void deletePort(Long id);

    public Port getPortById(Long id);

    public List<Port> listPort();

    public boolean existPortByIp(String ip);

    public boolean existPort(Long id);
}
