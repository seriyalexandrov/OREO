package com.oreo.database.service.port;

import com.oreo.database.model.port.Port;
import com.oreo.database.repositories.port.PortRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PortServiceImpl implements PortService {
    private PortRepository portRepository;

    @Autowired
    public PortServiceImpl(PortRepository portRepository) {
        this.portRepository = portRepository;
    }

    @Override
    public void savePort(Port port) {
        this.portRepository.save(port);
    }

    @Override
    public void deletePort(Long id) {
        this.portRepository.delete(id);
    }

    @Override
    public Port getPortById(Long id) {
        return this.portRepository.findOne(id);
    }

    @Override
    public List<Port> listPort() {
        List<Port> list = new ArrayList<>();
        this.portRepository.findAll().forEach(list::add);
        return list;
    }


    @Override
    public boolean existPortByIp(String ip) {
        List<Port> list = this.listPort();
        for (Port aport : list) {
            if ((aport.getIp().equals(ip))) {
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean existPort(Long id) {
        return portRepository.exists(id);
    }
}
