package com.oreo.database.service.user;

import com.oreo.database.model.user.User;

import java.util.List;

public interface UserService {
    public void saveUser(User user);

    public void deleteUser(Long id);

    public User getUserById(Long id)
            ;
    public User getUserByLogin(String login);

    public List<User> listUser();

    public boolean existUser(String name);

    public boolean existUser(Long id);
}
