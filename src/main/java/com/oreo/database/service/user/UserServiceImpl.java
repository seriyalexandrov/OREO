package com.oreo.database.service.user;

import com.oreo.database.model.user.User;
import com.oreo.database.repositories.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void saveUser(User user) {
        this.userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        this.userRepository.delete(id);
    }

    @Override
    public User getUserById(Long id) {
        return this.userRepository.findOne(id);
    }

    @Override
    public User getUserByLogin(String login) {
        List<User> users = listUser();
        for (User user : users) {
            if ((user.getName().equals(login))) {
                return user;
            }
        }
        return null;
    }

    @Override
    public List<User> listUser() {
        List<User> users = new ArrayList<>();
        this.userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public boolean existUser(String name) {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .anyMatch(user -> user.getName().equals(name));
    }

    @Override
    public boolean existUser(Long id) {
        return userRepository.exists(id);
    }
}
