package com.oreo.database.service.link;

import com.oreo.database.model.link.Link;
import com.oreo.database.repositories.link.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;


@Service
public class LinkServiceImpl implements LinkService {
    private LinkRepository linkRepository;

    @Autowired
    public LinkServiceImpl(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public void saveLink(Link link) {
        this.linkRepository.save(link);
    }

    @Override
    public void deleteLink(Long id) {
        this.linkRepository.delete(id);
    }

    @Override
    public Link getLinkById(Long id) {
        return this.linkRepository.findOne(id);
    }

    @Override
    public List<Link> listLink() {
        List<Link> links = new ArrayList<>();
        this.linkRepository.findAll().forEach(links::add);
        return links;
    }

    @Override
    public boolean existLinkByName(String name) {
        return StreamSupport.stream(linkRepository.findAll().spliterator(), false)
                .anyMatch(link -> link.getName().equals(name));
    }

    @Override
    public boolean existLink(Long id) {
        return linkRepository.exists(id);
    }

    @Override
    public boolean existLinkByIp(String source, String destination) {
        List<Link> links = this.listLink();
        if (links.isEmpty()) {
            return false;
        }
        for (Link link : links) {
            if (source.equals(link.getSource()) && destination.equals(link.getDestination())) {
                return true;
            }
            if (destination.equals(link.getSource()) && source.equals(link.getDestination())) {
                return true;
            }
        }
        return false;
    }
}
