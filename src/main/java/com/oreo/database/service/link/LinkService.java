package com.oreo.database.service.link;

import com.oreo.database.model.link.Link;

import java.util.List;

public interface LinkService {
    public void saveLink(Link link);

    public void deleteLink(Long id);

    public Link getLinkById(Long id);

    public List<Link> listLink();

    public boolean existLinkByName(String name);

    public boolean existLink(Long id);

    public boolean existLinkByIp(String source, String destination);
}
