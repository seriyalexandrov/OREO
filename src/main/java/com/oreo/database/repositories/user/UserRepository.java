package com.oreo.database.repositories.user;

import com.oreo.database.model.user.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
