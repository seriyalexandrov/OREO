package com.oreo.database.repositories.link;

import com.oreo.database.model.link.Link;
import org.springframework.data.repository.CrudRepository;


public interface LinkRepository extends CrudRepository<Link, Long> {
}
