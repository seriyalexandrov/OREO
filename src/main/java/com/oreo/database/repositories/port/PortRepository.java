package com.oreo.database.repositories.port;


import com.oreo.database.model.port.Port;
import org.springframework.data.repository.CrudRepository;

public interface PortRepository extends CrudRepository<Port, Long> {
}
