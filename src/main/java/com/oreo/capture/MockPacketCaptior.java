package com.oreo.capture;

import com.mongodb.util.JSON;
import com.oreo.collector.PackageCollector;
import com.oreo.database.model.link.Link;
import com.oreo.database.model.port.Port;
import com.oreo.database.service.link.LinkServiceImpl;
import com.oreo.database.service.port.PortServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MockPacketCaptior implements NetflowPacketCaptior {
    private static final Logger log = LoggerFactory.getLogger(MockPacketCaptior.class);

    //random generator of data
    private static final Random random = new Random();
    //flag which shows if we pause the capture
    private static boolean pause = false;

    @Autowired
    private PackageCollector packageCollector;

    @Autowired
    private PortServiceImpl portService;

    @Autowired
    LinkServiceImpl linkService;

    private long linkId = 0;


    public void run() {
        Map packet = new HashMap();
        while (true) {

            //here we simulate the speed of receiving packets
            try {
                Thread.currentThread().sleep(10);
            } catch (InterruptedException e) {
                log.error("", e);
            }

            //here we put random data
            packet.put("output_snmp", 5);
            packet.put("dst_as", random.nextInt(5));
            packet.put("dst_mask", random.nextInt(32));
            packet.put("in_pkts", 1);
            packet.put("dst_addr", "" + random.nextInt(2) + "." + random.nextInt(2) +
                    "." + random.nextInt(2) + "." + random.nextInt(2));
            packet.put("src_tos", 1);
            packet.put("first_switched", random.nextInt(100000));
            packet.put("unix_secs", System.currentTimeMillis() / 1000);
            packet.put("next_hop", "" + random.nextInt(2) + "." + random.nextInt(2) +
                    "." + random.nextInt(2) + "." + random.nextInt(2));
            packet.put("l4_src_port", random.nextInt(2));
            packet.put("src_mask", random.nextInt(32));
            packet.put("src_addr", "" + random.nextInt(2) + "." + random.nextInt(2) +
                    "." + random.nextInt(2) + "." + random.nextInt(2));
            packet.put("octet_count", random.nextInt(1000));
            packet.put("protocol", 17);
            packet.put("input_snmp", 3);
            packet.put("tcp_flags", 0);
            packet.put("last_switched", random.nextInt(100000));
            packet.put("I4_dst_port", random.nextInt(10000));
            packet.put("src_as", 2);
            packet.put("Count", random.nextInt(6));
            packet.put("SysUpTime", System.currentTimeMillis());
            packet.put("SequenceNum", random.nextInt(10000));
            packet.put("Sourcseid", 0);


            /**
             * here we try to add interfaces to db
             */

            String source = (String) packet.get("src_addr");
            String nextHop = (String) packet.get("next_hop");
            if (!portService.existPortByIp(source)) {
                portService.savePort(new Port(source));
            }

            if(!linkService.existLinkByIp(source, nextHop))
                linkService.saveLink(new Link("Link " + linkId++, source, nextHop));


            /**here we make json and put in bd
            if paused we do not put
             */
            if (!pause)
                packageCollector.writeDoc(makeJson(packet));
            packet.clear();


        }

    }

    private String makeJson(Map<String, Object> map) {
        return JSON.serialize(map);
    }

    @Override
    public void pause() {
        pause = true;
    }

    @Override
    public void cont() {
        pause = false;
    }
}
