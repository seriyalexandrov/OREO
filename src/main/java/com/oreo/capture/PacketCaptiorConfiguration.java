package com.oreo.capture;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PacketCaptiorConfiguration {

    @Value("${capture.network.device}")
    private int networkDevice;

    @Bean
    @ConditionalOnProperty(name = "use.mock.captior.netflow", havingValue = "true")
    public NetflowPacketCaptior mockPacketCaptior() {
        return new MockPacketCaptior();
    }

    @Bean
    @ConditionalOnProperty(name = "use.mock.captior.netflow", havingValue = "false", matchIfMissing = true)
    public NetflowPacketCaptior realPacketCaptior() {
        return new PacketCaptior(networkDevice);
    }


}
