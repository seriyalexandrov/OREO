package com.oreo.SearchInDB;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;

@Component
public class MongoQueryMaker {


    BasicDBObject makeSearchLinkTraficQuery(String ipSource, String ipDestination, long timeFromDelta, long timeToDelta, long time) throws UnknownHostException {
        BasicDBObject basicDBObject = new BasicDBObject();

        if(ipDestination.equals("nexthop"))
            basicDBObject.put("next_hop", ipSource);
        else {
            basicDBObject.put("src_addr", ipSource);
        }

        //We need to check whether ip destination not none
        if (!ipDestination.matches("[A-Za-z]*"))
            basicDBObject.put("dst_addr", ipDestination);
        basicDBObject.put("unix_secs", BasicDBObjectBuilder.start("$gte", time - timeFromDelta)
                .add("$lte", time - timeToDelta).get());
        return basicDBObject;
    }

    BasicDBObject[] makeSearchTraficBetween(String ipSource, String ipDestination, long from, long to, int numOfPoints, long timeDelta) {
        BasicDBObject[] basicDBObjects = new BasicDBObject[numOfPoints];
        for (int i = 0; i < numOfPoints; i++) {
            basicDBObjects[i] = new BasicDBObject();
            if(ipDestination.equals("nexthop"))
                basicDBObjects[i].put("next_hop", ipSource);
            else{
                basicDBObjects[i].put("src_addr", ipSource);}
            if (!ipDestination.matches("[A-Za-z]*"))
                basicDBObjects[i].put("dst_addr", ipDestination);
            basicDBObjects[i].put("unix_secs", BasicDBObjectBuilder.start("$gte", to - (i + 1) * timeDelta)
                    .add("$lte", to - i * timeDelta).get());
        }
        return basicDBObjects;
    }
}
