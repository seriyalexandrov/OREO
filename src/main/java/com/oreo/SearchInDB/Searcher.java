package com.oreo.SearchInDB;

import com.mongodb.BasicDBObject;
import com.oreo.collector.PackageCollector;
import com.oreo.database.service.link.LinkServiceImpl;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.ArrayList;

@Service
public class Searcher {

    @Autowired
    PackageCollector packageCollector;

    @Autowired
    MongoQueryMaker mongoQueryMaker;

    @Autowired
    LinkServiceImpl linkService;

    @Value("${bandwidth}")
    Integer bandwidth;

    public long getCurrentUtilization(String ipSource, String ipDestination, long deltaTime, boolean reverse) throws UnknownHostException {
        long res = 0;
        long time = TimeHelper.getTimeInSec();

        BasicDBObject basicDBObject = mongoQueryMaker.makeSearchLinkTraficQuery(ipSource, ipDestination, deltaTime, 0, time);
        for (Document doc : packageCollector.getCollection().find(basicDBObject)) {
            res += doc.getInteger("octet_count");
        }

        if (reverse) {
            BasicDBObject basicReverseDBObject = mongoQueryMaker.makeSearchLinkTraficQuery(ipDestination, ipSource, deltaTime, 0, time);
            for (Document doc : packageCollector.getCollection().find(basicReverseDBObject)) {
                res += doc.getInteger("octet_count");
            }
        }
        return res / deltaTime / bandwidth;
    }


    public ArrayList<Long> getStartUtilization(String ipSource, String ipDestination, int numOfPoints, long deltaTime, boolean reverse) {
        long dateTo = TimeHelper.getTimeInSec();
        long dateFrom = dateTo - numOfPoints * deltaTime;
        return getCustomUtilization(ipSource, ipDestination, dateFrom, dateTo, numOfPoints, reverse);
    }


    public ArrayList<Long> getDaysUtilization(String ipSource, String ipDestination, int numOfPoints, int numOfDays, boolean reverse) {
        ArrayList<Long> points = new ArrayList<>();
        long dateTo = TimeHelper.getTimeInSec();
        long dateFrom = dateTo - TimeHelper.getSecondsInHour() * TimeHelper.getHoursInDay() * numOfDays;
        return getCustomUtilization(ipSource, ipDestination, dateFrom, dateTo, numOfPoints, reverse);
    }


    public ArrayList<Long> getCustomUtilization(String ipSource, String ipDestination, long dateFrom, long dateTo, int numOfPoints, boolean reverse) {
        ArrayList<Long> points = new ArrayList<>();
        long res = 0;
        long timeDelta = (dateTo - dateFrom) / numOfPoints;
        BasicDBObject[] basicDBObjects = mongoQueryMaker.makeSearchTraficBetween(ipSource, ipDestination, dateFrom, dateTo, numOfPoints, timeDelta);
        BasicDBObject[] basicDBObjectsReverse = null;
        if (reverse)
            basicDBObjectsReverse = mongoQueryMaker.makeSearchTraficBetween(ipDestination, ipSource, dateFrom, dateTo, numOfPoints, timeDelta);

        for (int i = 0; i < numOfPoints; i++) {
            for (Document doc : packageCollector.getCollection().find(basicDBObjects[i])) {
                res += doc.getInteger("octet_count");
            }

            if (reverse) {
                for (Document doc : packageCollector.getCollection().find(basicDBObjectsReverse[i])) {
                    res += doc.getInteger("octet_count");
                }
            }
            points.add(res / timeDelta / bandwidth);
            res = 0;
        }

        return points;
    }

    public ArrayList<Long> getStartLinkUtilization(String ipSource, String ipDestination, int numOfPoints, long deltaTime) {
        ArrayList<Long> a = getStartUtilization(ipSource, "nexthop", numOfPoints, deltaTime, false);
        ArrayList<Long> b = getStartUtilization(ipDestination, "nexthop", numOfPoints, deltaTime, false);
        return sumTwo(a, b);
    }


    public long getCurrentLinkUtilization(String ipSource, String ipDestination, long deltaTime) throws UnknownHostException {
        long res = getCurrentUtilization(ipSource, "nexthop", deltaTime, false);
        res += getCurrentUtilization(ipDestination, "nexthop", deltaTime, false);
        return res;
    }

    public ArrayList<Long> getDaysLinkUtilization(String ipSource, String ipDestination, int numOfPoints, int numOfDays) {
        ArrayList<Long> a = getDaysUtilization(ipSource, "nexthop", numOfPoints, numOfDays, false);
        ArrayList<Long> b = getDaysUtilization(ipDestination, "nexthop", numOfPoints, numOfDays, false);
        return sumTwo(a, b);
    }


    public ArrayList<Long> getCustomLinkUtilization(String ipSource, String ipDestination, long dateFrom, long dateTo, int numOfPoints) {
        ArrayList<Long> a = getCustomUtilization(ipSource, "nexthop", dateFrom, dateTo, numOfPoints, false);
        ArrayList<Long> b = getCustomUtilization(ipDestination, "nexthop", dateFrom, dateTo, numOfPoints, false);
        return sumTwo(a, b);
    }


    private ArrayList sumTwo(ArrayList<Long> a, ArrayList<Long> b) {
        int i = 0;
        for (Long bytes : b) { a.set(i, a.get(i++) + bytes); }
        return a;
    }

}
