package com.oreo.SearchInDB;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TimeHelper {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static  final long secondsInHour = 3600;
    private static  final int hoursInDay = 24;

    public static long getTimeInSec(){
        return System.currentTimeMillis() / 1000L;
    }

    public static long getDeltaTime(int numOfPoints, int numOfDays) {
        return hoursInDay * secondsInHour / numOfPoints * numOfDays;
    }

    public static long parseTimeInSeconds(String date) throws ParseException {
        return dateFormat.parse(date).getTime() / 1000L;
    }

    public static long getSecondsInHour() {
        return secondsInHour;
    }

    public static int getHoursInDay() {
        return hoursInDay;
    }
}
