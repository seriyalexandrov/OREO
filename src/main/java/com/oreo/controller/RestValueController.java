package com.oreo.controller;

import com.oreo.SearchInDB.Searcher;
import com.oreo.SearchInDB.TimeHelper;
import com.mongodb.client.MongoCollection;
import com.oreo.Application;
import com.oreo.adapters.SnmpAdapter;
import com.oreo.capture.NetflowPacketCaptior;
import com.oreo.collector.PackageCollector;
import com.oreo.database.model.link.Link;
import com.oreo.database.model.port.Port;
import com.oreo.database.model.user.User;
import com.oreo.database.service.link.LinkServiceImpl;
import com.oreo.database.service.port.PortServiceImpl;
import com.oreo.database.service.user.UserServiceImpl;
import com.oreo.domain.SnifferConfiguration;
import com.oreo.domain.SnmpData;
import com.oreo.model.SimpleAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping("/rest")
@CrossOrigin
public class RestValueController {
//    private static final Logger log = LoggerFactory.getLogger(RestValueController.class);


    @Autowired
    private PackageCollector packageCollector;

    private final PasswordEncoder passwordEncoder;


    @Autowired
    private LinkServiceImpl linkService;

    @Autowired
    private PortServiceImpl portService;

    private UserServiceImpl userService;

    @Autowired
    private Searcher searcher;

    private SnifferConfiguration snifferConfiguration = new SnifferConfiguration("stopped");

    private final List<SseEmitter> sseEmitters = Collections.synchronizedList(new ArrayList<>());
    private SnmpData snmpData;
    private SnmpAdapter snmpAdapter;

    @Value("${delta.time.running}")
    private Long deltaTimeRunnig;

    @Value("${delta.time.days}")
    private Long deltaTimeDays;

    //necessary fields to start capture packets
    private Thread captureThread = null;
    private NetflowPacketCaptior packetCaptior;

    private static final Logger log = LoggerFactory.getLogger(RestValueController.class);

    @Autowired
    public RestValueController(NetflowPacketCaptior packetCaptior, SnmpAdapter adapter, PasswordEncoder passwordEncoder, //UserService userService,
                               UserServiceImpl userService) {
        this.snmpAdapter = adapter;
        this.snmpAdapter.start();
        this.packetCaptior = packetCaptior;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.userService.saveUser(new User("Admin", "Admin", "Admin"));
    }

    private void sendEvent(Updates updates) {
        synchronized (this.sseEmitters) {
            for (SseEmitter sseEmitter : this.sseEmitters) {
                try {
                    sseEmitter.send(updates, MediaType.APPLICATION_JSON);
                    sseEmitter.complete();
                } catch (Exception ignored) {
                    log.error("", ignored);
                }
            }
        }
    }

    @PostMapping
    public SnifferConfiguration create(@RequestBody @Valid SnifferConfiguration snifferConfiguration) {
        sendEvent(Updates.UPDATE_SNMP);
        return snifferConfiguration;
    }

    @RequestMapping(value = "/checkLog", method = RequestMethod.GET)
    public boolean checkLogin(@RequestParam String name, @RequestParam String password) {
        return true;
    }

    @RequestMapping(value = "/sniffState", method = RequestMethod.GET)
    public SnifferConfiguration value() {
        return this.snifferConfiguration;
    }

    @RequestMapping(value = "/sniffState/doRun", method = RequestMethod.GET)
    public SnifferConfiguration doRun(@RequestParam int port) {
        if (captureThread == null) {
            captureThread = new Thread(packetCaptior);
            captureThread.start();
        }
        packetCaptior.cont();
        this.snifferConfiguration.callRun(port);
        sendEvent(Updates.UPDATE_SNIFFER);
        return this.snifferConfiguration;
    }

    @RequestMapping(value = "/sniffState/doStop", method = RequestMethod.GET)
    public SnifferConfiguration doStop(@RequestParam int port) {
        this.snifferConfiguration.callStop(port);
        sendEvent(Updates.UPDATE_SNIFFER);
        packetCaptior.pause();
        return this.snifferConfiguration;
    }



    @RequestMapping(value = "/sniffState/subscribe", method = RequestMethod.GET)
    public SseEmitter subscribe() {
        SseEmitter sseEmitter = new SseEmitter();
        synchronized (this.sseEmitters) {
            this.sseEmitters.add(sseEmitter);
            sseEmitter.onCompletion(() -> {
                synchronized (RestValueController.this.sseEmitters) {
                    RestValueController.this.sseEmitters.remove(sseEmitter);
                }
            });
            sseEmitter.onTimeout(sseEmitter::complete);
        }
        return sseEmitter;
    }


    @RequestMapping(value = "/update/snmp", method = RequestMethod.GET)
    public SnmpData updateSnmp() {
        return this.snmpData;
    }


    @RequestMapping(value = "/interface/current_stat", method = RequestMethod.GET)
    public Long getCurrentAverageTrafic(@RequestParam String ipSource,
                                        @RequestParam(value = "ipDestination", required = false, defaultValue = "none") String ipDestination,
                                        @RequestParam(value = "deltaTime", required = false, defaultValue = "2") long deltaTime,
                                        @RequestParam(value = "reverse", required = false, defaultValue = "false") boolean reverse) {
        try {
            return searcher.getCurrentUtilization(ipSource, ipDestination, deltaTime, reverse);
        } catch (Exception e) {
            log.error("Error request", e);
            return null;
        }
    }

    @RequestMapping(value = "/interface/current_stat/init", method = RequestMethod.GET)
    public List<Long> getInitPointsNetflowTrafic(@RequestParam String ipSource,
                                                 @RequestParam(value = "ipDestination", required = false, defaultValue = "none") String ipDestination,
                                                 @RequestParam int numOfPoints,
                                                 @RequestParam(value = "deltaTime", required = false, defaultValue = "2") long deltaTime,
                                                 @RequestParam(value = "reverse", required = false, defaultValue = "false") boolean reverse) {
        try {
            return searcher.getStartUtilization(ipSource, ipDestination, numOfPoints, deltaTime, reverse);
        } catch (Exception e) {
            log.error("Error request", e);
            return null;
        }
    }

    @RequestMapping(value = "/interface/list/days", method = RequestMethod.GET)
    public List<Long> getDaysNetFlowTrafic(@RequestParam String ipSource,
                                           @RequestParam(value = "ipDestination", required = false, defaultValue = "none") String ipDestination,
                                           @RequestParam int numOfDays,
                                           @RequestParam(value = "numOfPoints", required = false, defaultValue = "24") int numOfPoints,
                                           @RequestParam(value = "reverse", required = false, defaultValue = "false") boolean reverse) {
        try {
            return searcher.getDaysUtilization(ipSource, ipDestination, numOfPoints, numOfDays, reverse);

        } catch (Exception e) {
            log.error("Error request", e);
            return null;
        }
    }


    @RequestMapping(value = "/interface/list/customDays", method = RequestMethod.GET)
    public List<Long> getDaysNetFlowTraficFromTo(@RequestParam String ipSource,
                                                 @RequestParam(value = "ipDestination", required = false, defaultValue = "none") String ipDestination,
                                                 @RequestParam String dateFrom,
                                                 @RequestParam String dateTo,
                                                 @RequestParam int numOfPoints,
                                                 @RequestParam(value = "reverse", required = false, defaultValue = "false") boolean reverse) {
        try {
            long timeFrom = TimeHelper.parseTimeInSeconds(dateFrom);
            long timeTo = TimeHelper.parseTimeInSeconds(dateTo);
            return searcher.getCustomUtilization(ipSource, ipDestination, timeFrom, timeTo, numOfPoints, reverse);
        } catch (ParseException e) {
            log.error("wrong data", e);
            return null;
        }
    }


    @RequestMapping(value = "/link/current_stat", method = RequestMethod.GET)
    public Long getCurrentAverageTrafic(@RequestParam String ipSource,
                                        @RequestParam String ipDestination,
                                        @RequestParam(value = "deltaTime", required = false, defaultValue = "2") long deltaTime) {
        try {
            return searcher.getCurrentLinkUtilization(ipSource, ipDestination, deltaTime);
        } catch (Exception e) {
            log.error("Error request", e);
            return null;
        }

    }


    @RequestMapping(value = "/link/current_stat/init", method = RequestMethod.GET)
    public List<Long> getInitPointsNetflowTrafic(@RequestParam String ipSource,
                                                 @RequestParam String ipDestination,
                                                 @RequestParam int numOfPoints,
                                                 @RequestParam(value = "deltaTime", required = false, defaultValue = "2") long deltaTime) {
        try {
            return searcher.getStartLinkUtilization(ipSource, ipDestination, numOfPoints, deltaTime);
        } catch (Exception e) {
            log.error("Error request", e);
            return null;
        }
    }


    @RequestMapping(value = "/link/list/days", method = RequestMethod.GET)
    public List<Long> getDaysNetFlowTraficLinks(@RequestParam String ipSource,
                                                @RequestParam String ipDestination,
                                                @RequestParam int numOfDays,
                                                @RequestParam(value = "numOfPoints", required = false, defaultValue = "24") int numOfPoints) {
        try {
            return searcher.getDaysLinkUtilization(ipSource, ipDestination, numOfPoints, numOfDays);
        } catch (Exception e) {
            log.error("Error request", e);
            return null;
        }
    }


    @RequestMapping(value = "/link/list/customDays", method = RequestMethod.GET)
    public List<Long> getDaysNetFlowTraficFromToLinks(@RequestParam String ipSource,
                                                      @RequestParam String ipDestination,
                                                      @RequestParam String dateFrom,
                                                      @RequestParam String dateTo,
                                                      @RequestParam int numOfPoints) {
        try {
            long timeFrom = TimeHelper.parseTimeInSeconds(dateFrom);
            long timeTo = TimeHelper.parseTimeInSeconds(dateTo);
            return searcher.getCustomLinkUtilization(ipSource, ipDestination, timeFrom, timeTo, numOfPoints);
        } catch (ParseException e) {
            log.error("wrong data", e);
            return null;
        }
    }

    @RequestMapping(value = "/interface/list", method = RequestMethod.GET)
    public List<Port> getAllPorts() {
        try {
            return portService.listPort();
        } catch (Exception e) {
            log.error("Empty list", e);
            return null;
        }

    }


    @EventListener
    public void recieveSnmpData(SnmpData data) {
        this.snmpData = data;
        sendEvent(Updates.UPDATE_SNMP);
    }

    @RequestMapping(value = "/link/list", method = RequestMethod.GET)
    public List<Link> getAllLinks() {
        try {
            return linkService.listLink();
        } catch (Exception e) {
            log.error("Empty list", e);
            return null;
        }
    }

    @RequestMapping(value = "/user/new", method = RequestMethod.GET)
    public boolean addNewUser(@RequestParam String name, @RequestParam String password) {
        if (userService.existUser(name)) {
            return false;
        }
        User user = new User(name, password, "Admin");
        userService.saveUser(user);
        sendEvent(Updates.UPDATE_USER);
        return true;
    }

    @RequestMapping(value = "/link/delete", method = RequestMethod.GET)
    public boolean deleteLink(@RequestParam Long id) {
        linkService.deleteLink(id);
        sendEvent(Updates.UPDATE_LINK);
        return !linkService.existLink(id);
    }

    @RequestMapping(value = "/user/delete", method = RequestMethod.GET)
    public boolean deleteUser(@RequestParam Long id) {
        userService.deleteUser(id);
        sendEvent(Updates.UPDATE_USER);
        return !userService.existUser(id);
    }


    @RequestMapping(value = "/user/edit", method = RequestMethod.GET)
    public boolean updateUser(@RequestParam Long id, @RequestParam String name, @RequestParam String password) {
        if (userService.existUser(id)) {
            return false;
        }
        User user = userService.getUserById(id);
        user.setName(name);
        user.setPassword(password);
        sendEvent(Updates.UPDATE_USER);
        return true;
    }
    @EventListener
    public void addNetflowData(MongoCollection docs) {
//        netFlowData.clear();
//        for (Object obj : docs.find().sort(new Document("_id", -1)).limit(100)) {
//            Document document = (Document) obj;
//            int secs = document.getInteger("UnixSecs");
//
//            Date date = new Date((long)secs * 1000);
//            String ipSource = document.getString("ipv4_src_addr");
//            String ipDestination = document.getString("ipv4_dst_addr");
//            int bytes = document.getInteger("in_bytes");
//            NetFlowData data = new NetFlowData(ipSource, ipDestination, bytes, date.toString());
//            netFlowData.add(data);
//        }
//
//        sendCreate(Updates.UPDATE_NETFLOW);
    }

    @GetMapping("/authenticate")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void authenticate() {
        // we don't have to do anything here
        // this is just a secure endpoint and the JWTFilter
        // validates the token
        // this service is called at startup of the app to check
        // if the jwt token is still valid
    }

@RequestMapping(value = "/registrate/doLogin", method = RequestMethod.GET)
public boolean registrate(@RequestParam String login, @RequestParam String password) {
    User user = userService.getUserByLogin(login);
    if (user != null) {
        if (user.getPassword().equals(password)) {
            SecurityContextHolder.getContext().setAuthentication(new SimpleAuthentication(user));
            com.oreo.Application.logger.info("This user was authenticated: " + login + " as " + user.getAuthorities().getAuthority());
            return true;
        }
        com.oreo.Application.logger.info("This user was not authenticated: " + login);
        return false;
    }
    com.oreo.Application.logger.info("This user does not exist: " + login);
    return false;
    }
}