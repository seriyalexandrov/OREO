package com.oreo.controller;

public enum Updates {
    UPDATE_SNIFFER,
    UPDATE_SNMP,
    UPDATE_LINK,
    UPDATE_USER;
}
