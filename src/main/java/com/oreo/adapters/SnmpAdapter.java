package com.oreo.adapters;

import org.springframework.stereotype.Service;

@Service
public abstract class SnmpAdapter extends Thread {
    public abstract void update(String string);

}
