package com.oreo.adapters;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SnmpAdapterConfiguration {

    public final ApplicationEventPublisher eventPublisher;

    public SnmpAdapterConfiguration(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Bean
    @ConditionalOnProperty(name = "use.mock.captior.snmp", havingValue = "false", matchIfMissing =  true)
    public SnmpAdapter realAdapter(){return new SnmpAdapterReal(eventPublisher);}


    @Bean
    @ConditionalOnProperty(name = "use.mock.captior.snmp", havingValue = "true")
    public MockSnmpAdapter mockAdapter(){
        return new MockSnmpAdapter(eventPublisher);}

}
