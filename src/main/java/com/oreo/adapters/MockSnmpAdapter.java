package com.oreo.adapters;

import com.oreo.domain.SnmpData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Random;


public class MockSnmpAdapter extends SnmpAdapter {
    private static final Logger log = LoggerFactory.getLogger(MockSnmpAdapter.class);


    public final ApplicationEventPublisher eventPublisher;

    @Autowired
    public MockSnmpAdapter(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }


    public void update(String bandWidth) {
        SnmpData snmpData = new SnmpData(bandWidth);
        eventPublisher.publishEvent(snmpData);
    }

    @Override
    public void run() {

        while (true) {
            try {
                this.sleep(10000);
                //here we generate an random data to send to client
                update(String.valueOf(new Random().nextInt()));
            } catch (InterruptedException e) {
                log.error("", e);
            }
        }
    }
}
