package com.oreo.adapters;

import com.oreo.domain.SnmpData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.*;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.springframework.context.ApplicationEventPublisher;

import java.io.IOException;


public class SnmpAdapterReal extends SnmpAdapter {

    private static final Logger log = LoggerFactory.getLogger(SnmpAdapter.class);

    public final ApplicationEventPublisher eventPublisher;

    /*Создаем поток, который будет слушать нужный порт.*/
    private Snmp snmp = null;
    private Address targetAddress = GenericAddress.parse("udp:127.0.0.1/162");
    private TransportMapping transport = null;


    /*В конструкторе устанавливаем слушателя SNMP протокола.*/
    public SnmpAdapterReal(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;

        try {
            transport = new DefaultUdpTransportMapping();
        } catch (IOException e) {
            log.error("", e);
        }
        snmp = new Snmp(transport);

        CommandResponder trapPrinter = new CommandResponder() {
            public void processPdu(CommandResponderEvent pdu) {
                PDU command = pdu.getPDU();

                if (command != null) {

                    if (log.isDebugEnabled()) {
                        log.debug("Got PDU command " + command.get(0).toValueString());
                    }

                    update(command.get(0).toValueString());
                }
            }
        };
        snmp.addNotificationListener(targetAddress, trapPrinter);
    }

    /*Сам поток спит. От него лишь требуется держать в памяти наш обработчик trapPrinter*/
    @Override
    public void run() {
        while (true) {

            while (true) {
                try {
                    this.sleep(1000);
                } catch (InterruptedException e) {
                    log.error("", e);
                }
            }
        }
    }


    @Override
    public void update(String bandWidth) {
        System.out.println(1);
        SnmpData snmpData = new SnmpData(bandWidth);
        //here wee tell controller to send data to client
        eventPublisher.publishEvent(snmpData);
    }
}
