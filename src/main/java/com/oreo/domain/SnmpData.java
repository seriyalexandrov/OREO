package com.oreo.domain;


public class SnmpData {
    private final String snmpData;

    public SnmpData(String snmpData) {
        this.snmpData = snmpData;
    }

    public String getSnmpData() {
        return this.snmpData;
    }

}