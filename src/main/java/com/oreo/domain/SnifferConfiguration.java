package com.oreo.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class SnifferConfiguration implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(SnifferConfiguration.class);


    private String status;
    private int port;

    public SnifferConfiguration() {
    }

    public SnifferConfiguration(String status) {
        this.status = status;
        this.port = 0;
    }

    public SnifferConfiguration(String status, int port) {
        this.status = status;
        this.port = port;
    }

    public String getStatus() {
        return this.status;
    }

    public int getPort() {
        return this.port;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void callRun(int port) {
        if ("stopped".equals(this.getStatus())) {
            this.setStatus("running");
        }
        if (this.port != port) {
            this.setPort(port);
        }
    }

    public void callStop(int port) {
        if (this.getPort() == port) {
            if ("running".equals(this.getStatus())) {
                this.setStatus("stopped");
            }
        } else {
            throw new RuntimeException();
        }

    }
}
