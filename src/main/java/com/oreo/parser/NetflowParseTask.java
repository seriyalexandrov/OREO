package com.oreo.parser;

import com.mongodb.util.JSON;
import com.oreo.database.model.link.Link;
import com.oreo.database.model.port.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import v5.NetFlowV5Header;
import v5.NetFlowV5Packet;
import v5.NetFlowV5Parser;
import v5.NetFlowV5Record;

import java.util.Map;

/**
 * This is a class where our Threads parse packets
 * each Thread checks the Quee to get packet
 * if there is no packet it goes asleep for a millisecond
 */

public final class NetflowParseTask implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(NetflowParseTask.class);

    private final byte[] packetBytes;

    public NetflowParseTask(byte[] packetBytes) {
        this.packetBytes = packetBytes;
    }

    @Override
    public void run() {

        try {
            NetFlowV5Packet packet = NetFlowV5Parser.parsePacket(packetBytes);//get the data from received packet
            NetFlowV5Header header = packet.getHeader();
            for (NetFlowV5Record record : packet.getRecords())//Here we get json object from netflow data
            {
                Map map = record.toMap();
                /**
                 Here we add to our data data from  header
                 header has useful information we need to show
                 */
                map.putAll(header.toMap());
                /**
                 * Here we put ip into Interfaces database
                 * we need to check whether ip contains in bd
                 * and we need to synchronize if do not want to have problems with bd
                 */
                synchronized (NetFlowParser.executorService) {

                    String source = (String) map.get("src_addr");
                    String nextHop = (String) map.get("next_hop");
                    if (!NetFlowParser.portService.existPortByIp(source)) {
                        NetFlowParser.portService.savePort(new Port(source));
                    }

                    if(!NetFlowParser.linkService.existLinkByIp(source, nextHop))
                        NetFlowParser.linkService.saveLink(new Link("Link " + NetFlowParser.linkId++, source, nextHop));
                }

                /**
                 * Here we parse all our data into json
                 */

                NetFlowParser.packageCollector.writeDoc(makeJson(map));
            }
        } catch (Exception ignored) {//most of exceptions we should ignore to ensure constant work of parser
            log.error("", ignored);
        }

    }

    /**
     * In this method we parse our netflow data into json
     * First of all we try to get numbers (port_address, tcp_flags, dst_mask ,etc)
     * If we cannot parse it means that field is an ip address
     */
    private String makeJson(Map<String, Object> map) {
        return JSON.serialize(map);
    }
}