package com.oreo.parser;

import com.oreo.capture.PacketCaptior;
import com.oreo.collector.PackageCollector;
import com.oreo.database.service.link.LinkService;
import com.oreo.database.service.port.PortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * This class is made to make parse packet parallel
 * All fields are static cause we don't need to make them for each class
 * We need the num of Proc cause we avoid making extra threads and to avoid making less  we need
 */

@Component
public class NetFlowParser {
    static final Integer numOfProc = Runtime.getRuntime().availableProcessors();//we need this num to know the num of thread to create
    static final ExecutorService executorService = Executors.newFixedThreadPool(numOfProc);

    static PackageCollector packageCollector;
    static PortService portService;
    static LinkService linkService;
    static long linkId = 0;

    @Autowired
    public NetFlowParser(PackageCollector packageCollector, PortService portService, LinkService linkService) {
        NetFlowParser.packageCollector = packageCollector;
        NetFlowParser.portService = portService;
        NetFlowParser.linkService = linkService;
    }

    public static void stop() {
        executorService.shutdown();
    }

    public static void addPacket(byte[] packet) {
        //if sniffer is stoped we do not put in bd
        if (!PacketCaptior.isPause()) {
            if (!executorService.isShutdown())
                executorService.execute(new NetflowParseTask(packet));
        }
    }

}
