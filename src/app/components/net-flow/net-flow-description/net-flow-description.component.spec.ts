import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NetFlowDescriptionComponent} from './net-flow-description.component';

describe('NetFlowDescriptionComponent', () => {
    let component: NetFlowDescriptionComponent;
    let fixture: ComponentFixture<NetFlowDescriptionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NetFlowDescriptionComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NetFlowDescriptionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
