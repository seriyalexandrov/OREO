import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NetFlowComponent} from './net-flow.component';

describe('NetFlowComponent', () => {
    let component: NetFlowComponent;
    let fixture: ComponentFixture<NetFlowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NetFlowComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NetFlowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
