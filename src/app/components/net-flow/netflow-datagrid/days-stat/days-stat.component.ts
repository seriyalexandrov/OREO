import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';

declare var require: any;
const Highcharts = require('highcharts/highcharts.src');
import 'highcharts/adapters/standalone-framework.src';
import {NetflowDataService} from "../../../../services/netflow-data.service";
import {Subject} from "rxjs";

@Component({
    selector: 'app-days-stat',
    templateUrl: './days-stat.component.html',
    styleUrls: ['./days-stat.component.css']
})
export class DaysStatComponent implements OnInit {

    @ViewChild('chart') public chartEl: ElementRef;
    private _chart: any;
    destroy$: Subject<boolean> = new Subject<boolean>();

    constructor(private netflowService: NetflowDataService) {

    }

    ngOnInit() {

        this.netflowService.messages$.takeUntil(this.destroy$).subscribe((event) => {
            if (event && event.data && event.data.regime !== "realTime" && event.data.ip) {

                console.log("redrawStatic")
                this.netflowService.getStaticDataOfDays(event.data.ip, event.data.regime)
                    .subscribe(instance => {



                        if (this._chart && this._chart.series && this._chart.series.length >= 1) {
                            this._chart.get('stat').remove();
                            this.destroy$.next(true);
                        }
                        if (this._chart){
                            console.log("inside if");
                            this._chart.addSeries({
                                id: "stat",
                                data: (function () {
                                    var data = [],
                                        time = (new Date()).getTime(),
                                        i;

                                    var n = +event.data.regime;

                                    for (i = -23; i <= 0; i += 1) {
                                        data.push({
                                            x: time + i * n * 1000 * 3600,
                                            y: instance[23 + i]
                                        });
                                    }
                                    return data;
                                }())

                            })
                        }

                    })
            }
        })
    }


    public ngAfterViewInit() {

        let opts: any = {
            yAxis: {
                max: 100,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{
                    from: 85,
                    to: 85,
                    color: "rgba(255,0,0,0.30)"
                },
                    {
                        from: 50,
                        to: 50,
                        color: "rgba(0,255,0,0.30)"
                    }]
            },
            chart: {

                events: {
                    addSeries: function () {
                        var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                            .attr({
                                fill: Highcharts.getOptions().colors[0],
                                padding: 10,
                                r: 5,
                                zIndex: 8
                            })
                            .css({
                                color: '#FFFFFF'
                            })
                            .add();

                        setTimeout(function () {
                            label.fadeOut();
                        }, 1000);
                    }
                }

            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },

        };

        if (this.chartEl && this.chartEl.nativeElement) {
            opts.chart = {
                type: 'spline',
                backgroundColor: "#FAFAFA",
                renderTo: this.chartEl.nativeElement
            };

            this._chart = new Highcharts.Chart(opts);

        }
    }

    public ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
        this._chart.destroy();
    }
}
