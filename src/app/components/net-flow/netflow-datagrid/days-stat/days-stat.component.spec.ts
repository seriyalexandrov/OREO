import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaysStatComponent} from './days-stat.component';

describe('DaysStatComponent', () => {
    let component: DaysStatComponent;
    let fixture: ComponentFixture<DaysStatComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DaysStatComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DaysStatComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
