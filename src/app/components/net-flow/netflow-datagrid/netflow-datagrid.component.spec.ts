import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NetflowDatagridComponent} from './netflow-datagrid.component';

describe('NetflowDatagridComponent', () => {
    let component: NetflowDatagridComponent;
    let fixture: ComponentFixture<NetflowDatagridComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NetflowDatagridComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NetflowDatagridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
