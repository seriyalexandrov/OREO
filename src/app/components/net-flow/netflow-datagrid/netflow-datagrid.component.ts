import {Component, OnInit, Input} from '@angular/core';
import {NetflowDataService, IPsource} from "../../../services/netflow-data.service";
import {DatagridPagination, Datagrid} from "clarity-angular";

@Component({
    selector: 'app-netflow-datagrid',
    templateUrl: './netflow-datagrid.component.html',
    styleUrls: ['./netflow-datagrid.component.css'],

})
export class NetflowDatagridComponent implements OnInit {

    ipList: IPsource[];
    selectedIp: IPsource;


    isIpSelected: boolean;
    selectedRegime: string;

    list: string[];


    isRealTime: boolean

    constructor(private netflowService: NetflowDataService) {
        this.isIpSelected = false;
        this.selectedRegime = "realTime";
        this.isRealTime = true
    }

    ngOnInit() {
        this.netflowService.getIpSources()
            .subscribe(instance => {
                this.ipList = instance
            });
    }

    setRealTime(bool: boolean) {
        this.isRealTime = bool;
    }

    startTracking() {

        this.isIpSelected = false;




        this.isRealTime = (this.selectedRegime === "realTime");

        if (this.selectedIp) {
            this.isIpSelected = true;
            this.netflowService.setSelectedIp(this.selectedIp.ip)
            this.netflowService.messages$.next(
                {"data" :
                    {
                        "ip" :this.selectedIp.ip,
                        "regime":this.selectedRegime
                    }});

        }
    }

}
