import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';


declare var require: any;
const Highcharts = require('highcharts/highcharts.src');
import 'highcharts/adapters/standalone-framework.src';
import {NetflowDataService} from "../../../../services/netflow-data.service";
import {Observable, Subject} from "rxjs";


@Component({
    selector: 'app-realtime-stat',
    templateUrl: './realtime-stat.component.html',
    styleUrls: ['./realtime-stat.component.css'],

})

export class RealtimeStatComponent implements OnInit {

    destroy$: Subject<boolean> = new Subject<boolean>();
    initData: number[];
    newPoint: number;

    @ViewChild('chart') public chartEl: ElementRef;
    private _chart: any;

    constructor(private netflowService: NetflowDataService) {

    }

    ngOnInit(): void {

        this.netflowService.messages$.subscribe((event) => {
            if (event && event.data && event.data.regime === "realTime" && event.data.ip) {
                console.log("redraw");
                const ip = this.netflowService.getSelectedIp();


                this.netflowService.getInitData(ip, "10")
                    .subscribe(instance => {

                        if (this._chart && this._chart.series && this._chart.series.length >= 1) {
                            this._chart.get('stat').remove();
                            this.destroy$.next(true);
                        }


                        if (this._chart)
                            this._chart.addSeries({
                                showInLegend: false,
                                id: "stat",
                                data: (function () {
                                    var data = [],
                                        time = (new Date()).getTime(),
                                        i;

                                    for (i = -9; i <= 0; i += 1) {
                                        data.push({
                                            x: time + i * 2000,
                                            y: instance[9 + i]
                                        });
                                    }
                                    return data;
                                }())

                            })
                        Observable.interval(2000).takeUntil(this.destroy$)
                            .subscribe(val => {
                                if (this._chart) {

                                    const ip = this.netflowService.getSelectedIp();

                                    this.netflowService.getCurrentTraffic(ip)
                                        .subscribe(instance => {
                                            this.newPoint = instance;

                                            if (this._chart && this._chart['series'] && this._chart['series'][0]) {
                                                this._chart['series'][0].addPoint([(new Date()).getTime(), this.newPoint], true, true);
                                            }
                                        })
                                }
                            })
                    })
            }
        });
    }


    public ngAfterViewInit() {
        let opts: any = {
            yAxis: {
                max: 100,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{
                    from: 85,
                    to: 85,
                    color: "rgba(255,0,0,0.30)"
                },
                    {
                        from: 50,
                        to: 50,
                        color: "rgba(0,255,0,0.30)"
                    }]
            },
            chart: {
                events: {
                    addSeries: function () {
                        var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                            .attr({
                                fill: Highcharts.getOptions().colors[0],
                                padding: 10,
                                r: 5,
                                zIndex: 8
                            })
                            .css({
                                color: '#FFFFFF'
                            })
                            .add();

                        setTimeout(function () {
                            label.fadeOut();
                        }, 1000);
                    }
                }
            },

            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },

        };

        if (this.chartEl && this.chartEl.nativeElement) {
            opts.chart = {
                type: 'spline',
                backgroundColor: "#FAFAFA",
                reflow: true,

                renderTo: this.chartEl.nativeElement
            };

            this._chart = new Highcharts.Chart(opts);

        }
    }

    public ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
        this._chart.destroy();
    }
}
