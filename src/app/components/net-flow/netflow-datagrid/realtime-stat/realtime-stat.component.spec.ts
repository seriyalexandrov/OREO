import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RealtimeStatComponent} from './realtime-stat.component';

describe('RealtimeStatComponent', () => {
    let component: RealtimeStatComponent;
    let fixture: ComponentFixture<RealtimeStatComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RealtimeStatComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RealtimeStatComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
