import {Component, OnInit} from '@angular/core';
import {SnifferStateService, Sniffer} from '../../../services/snifferState.service'
import {SseService} from "../../../services/sse.service";

@Component({
    selector: 'sniffer-state',
    templateUrl: './snifferState.component.html',
    styleUrls: ['./snifferState.component.css'],
    providers: [SnifferStateService]
})


export class SnifferStateComponent implements OnInit {

    sniffer: Sniffer;
    portToExecute: string;

    constructor(private snifferService: SnifferStateService, private sseService: SseService) {
    }

    ngOnInit() {
        this.snifferService.getSnifferInstance()
            .subscribe(instance => {
                this.sniffer = instance
            });
        this.sseService.subscribe()
            .subscribe({
                next: (event) => {


                    if (event.data === "\"UPDATE_SNIFFER\"") {
                        console.log('observer: ' + event.data);
                        this.snifferService.getSnifferInstance()
                            .subscribe(instance => {
                                this.sniffer = instance
                            });
                    }
                }
            });
    }

    //Run the sniffer on the portToExecute from the inputForm!
    runSniffer(): void {
        this.snifferService.runSniffer(this.portToExecute)
            .subscribe(instance => {
                this.sniffer = instance
            });
    }

    stopSniffer(): void {
        this.snifferService.stopSniffer(this.sniffer.port)  //stop it on the port, which is analysed
            .subscribe(instance => {                        //by sniffer (!not the port to execute!)
                this.sniffer = instance                     //if we will use port to execute, REST
            });                                             //through an exeption
    }

    closeMessage: string = "";


}
