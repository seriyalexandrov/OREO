import {Component, OnInit} from '@angular/core';
import {LinksService, Link, LinkComparator,} from "../../services/links.service";
import {StatisticService} from "../../services/statistic.service";
import {Router} from "@angular/router";


@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css'],

})
export class SettingsComponent implements OnInit {
    links: Link[] = [];

    router: Router;
    selectedLink: Link;
    linkComparator = new LinkComparator();
    isEdit = false;
    inName: string;
    maxUtilization = 0;

    constructor(_router: Router, private linksService: LinksService, private statisticService: StatisticService) {
        this.router = _router;
    }

    ngOnInit() {
        this.linksService.getLinks()
            .subscribe(instance => {
                this.links = instance
            });
    }

    getAllowedLinks(): Link[] {
        let links = new Array<Link>();
        for (let a of this.links) {
            if (parseInt(a.utilization) >= this.maxUtilization) {
                links.push(a)
            }

        }
        return links;
    }

    goToStatistics(selectAndGo: boolean) {                 //true - selectLinkAndGo  false - just back
        if (selectAndGo) {
            this.statisticService.setSelectedLink(this.selectedLink);
        }
        this.router.navigateByUrl('/statistics');
    }

    refresh() {
        this.linksService.getLinks()
            .subscribe(instance => {
                this.links = instance
            });
    }

    renameLink() {
        let i = this.links.indexOf(this.selectedLink);
        this.linksService.updateLink(this.selectedLink.id, this.inName)
            .subscribe(instance => {
                    this.links[i] = instance;
                }
            );
    }

    showDialog(): void {
        this.isEdit = !this.isEdit;
    }

    clearInput(): void {
        this.inName = "";
    }
}
