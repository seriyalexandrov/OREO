import {Component, Input, OnInit} from '@angular/core';
import {SecurityService} from '../../services/security.service';
import {User} from '../../user';

@Component({
    selector: 'app-security',
    templateUrl: './security.component.html',
    styleUrls: ['./security.component.css'],
    providers: [SecurityService]
})
export class SecurityComponent implements OnInit {

    user = new User("", "");

    test: Boolean;


    constructor(private securityService: SecurityService) {
    }

    ngOnInit() {
    }

    get diagnostic() {
        return JSON.stringify(this.user);
    }

    sendUser(): void {
        this.securityService.sendUser(this.user.name, this.user.password)
            .subscribe(instance => {
                this.test = instance
            });
    }
}
