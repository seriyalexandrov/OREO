import {Component, OnInit} from '@angular/core';
import {SseService} from "../../services/sse.service";

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

    constructor(private sseService: SseService) {
    }

    ngOnInit() {
        this.sseService.subscribe()
            .subscribe({
                next: (event) => {
                }
            });
    }

}
