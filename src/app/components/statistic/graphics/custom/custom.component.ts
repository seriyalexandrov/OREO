import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';

declare var require: any;
const Highcharts = require('highcharts/highcharts.src');
import 'highcharts/adapters/standalone-framework.src';
import {StatisticService} from "../../../../services/statistic.service";
import {Subject} from "rxjs";
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent implements OnInit {

    @ViewChild('chart') public chartEl: ElementRef;
    private _chart: any;
    destroy$: Subject<any> = new Subject();


    dateFrom: Date;
    dateTo: Date;
    numOfPoints: string;

    constructor(private statisticService: StatisticService) {

    }

    ngOnInit() {

        this.statisticService.customMessages$.takeUntil(this.destroy$).subscribe((event) => {
            if (event && event.data){

                console.log("got message");

                const ipSource = this.statisticService.getIpSource();
                const ipDestination = this.statisticService.getIpDestination();
                this.dateFrom = this.statisticService.getDateFrom();
                this.dateTo = this.statisticService.getDateTo();
                this.numOfPoints = this.statisticService.getCustomNumOfPoints();


                var dateToInMillis   = Date.parse(this.dateTo.toString()),
                    dateFromInMillis = Date.parse(this.dateFrom.toString());

                var totalNumOfPoints = +this.numOfPoints * (dateToInMillis - dateFromInMillis) / (1000*60*60*24);




                this.statisticService.getCustomStaticData(ipSource, ipDestination, this.dateFrom.toString(), this.dateTo.toString(), totalNumOfPoints.toString())
                    .subscribe(instance => {

                        if (this._chart && this._chart.series && this._chart.series.length >= 1) {
                            this._chart.get('stat').remove();
                            this.destroy$.next(true);
                        }
                        if (this._chart){
                            this._chart.addSeries({
                                id: "stat",
                                data: (function () {

                                    var currentDate = (new Date()).getTime();

                                    var data = [], i;

                                    var count = instance.length;
                                    var delta = dateToInMillis - dateFromInMillis;

                                    for (i = -count + 1; i <= 0; i += 1) {
                                        data.push({
                                            x: dateToInMillis + (delta * i)/(count - 1),   // i=-23 x = timeFrom || i = 0 x = timeTo
                                            y: instance[count - 1 + i]
                                        });
                                    }
                                    return data;
                                }())

                            })
                        }
                    })
            }

        });

    }


    public ngAfterViewInit() {

        let opts: any = {
            title: {
                text: 'Utilization of link'
            },
            yAxis: {
                max: 100,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{
                    from: 85,
                    to: 85,
                    color: "rgba(255,0,0,0.30)"
                },
                    {
                        from: 50,
                        to: 50,
                        color: "rgba(0,255,0,0.30)"
                    }]
            },
            chart: {

                events: {
                    addSeries: function () {
                        var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                            .attr({
                                fill: Highcharts.getOptions().colors[0],
                                padding: 10,
                                r: 5,
                                zIndex: 8
                            })
                            .css({
                                color: '#FFFFFF'
                            })
                            .add();

                        setTimeout(function () {
                            label.fadeOut();
                        }, 1000);
                    }
                }

            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },

        };

        if (this.chartEl && this.chartEl.nativeElement) {
            opts.chart = {
                type: 'spline',
                backgroundColor: "#FAFAFA",
                renderTo: this.chartEl.nativeElement
            };

            this._chart = new Highcharts.Chart(opts);

        }
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        this._chart.destroy();
    }
}
