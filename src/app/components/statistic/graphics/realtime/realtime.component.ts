import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';


declare var require: any;
const Highcharts = require('highcharts/highcharts.src');
import 'highcharts/adapters/standalone-framework.src';

import {StatisticService} from "../../../../services/statistic.service";
import {Observable, Subject} from "rxjs";
import {NotificationsService} from "../../../../services/notifications.service";


@Component({
    selector: 'app-realtime',
    templateUrl: './realtime.component.html',
    styleUrls: ['./realtime.component.css'],

})
export class RealtimeComponent implements OnInit {

    destroy$: Subject<any> = new Subject();

    initData: number[];
    newPoint: number;

    @ViewChild('chart') public chartEl: ElementRef;
    private _chart: any;

    constructor(private statisticService: StatisticService, private notificationService: NotificationsService) {
    }

    ngOnInit(): void {


        const ipSource = this.statisticService.getIpSource();
        const ipDestination = this.statisticService.getIpDestination();


        this.statisticService.getInitData(ipSource, ipDestination, "10")
            .subscribe(instance => {

                if (this._chart && this._chart.series && this._chart.series.length >= 1) {
                    this._chart.get('stat').remove();
                    this.destroy$.next(true);
                }


                if (this._chart)
                    this._chart.addSeries({
                        showInLegend: false,
                        id: "stat",
                        data: (function () {
                            var data = [],
                                time = (new Date()).getTime(),
                                i;

                            for (i = -9; i <= 0; i += 1) {
                                data.push({
                                    x: time + i * 2000,
                                    y: instance[9 + i]
                                });
                            }
                            return data;
                        }())

                    })
                Observable.interval(2000).takeUntil(this.destroy$)
                    .subscribe(val => {
                        if (this._chart) {

                            this.statisticService.getCurrentUtilization(ipSource, ipDestination)
                                .subscribe(instance => {
                                    this.newPoint = instance;

                                    if (instance > 85) {
                                        this.notificationService.addNotification("High Utilization");
                                    }

                                    if (this._chart && this._chart['series'] && this._chart['series'][0]) {
                                        this._chart['series'][0].addPoint([(new Date()).getTime(), this.newPoint], true, true);
                                    }
                                })
                        }
                    })
            })

    }


    public ngAfterViewInit() {
        let opts: any = {
            title: {
                text: 'Utilization of link: ' + this.statisticService.getSelectedLink().name,
            },
            yAxis: {
                max: 100,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{
                    from: 85,
                    to: 85,
                    color: "rgba(255,0,0,0.30)"
                },
                    {
                        from: 50,
                        to: 50,
                        color: "rgba(0,255,0,0.30)"
                    }]
            },
            chart: {
                events: {
                    addSeries: function () {
                        var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                            .attr({
                                fill: Highcharts.getOptions().colors[0],
                                padding: 10,
                                r: 5,
                                zIndex: 8
                            })
                            .css({
                                color: '#FFFFFF'
                            })
                            .add();

                        setTimeout(function () {
                            label.fadeOut();
                        }, 1000);
                    }
                },

            },

            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },

        };

        if (this.chartEl && this.chartEl.nativeElement) {
            opts.chart = {
                type: 'spline',
                backgroundColor: "#FAFAFA",
                reflow: true,


                renderTo: this.chartEl.nativeElement
            };

            this._chart = new Highcharts.Chart(opts);

        }
    }
    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        this._chart.destroy();
    }

}
