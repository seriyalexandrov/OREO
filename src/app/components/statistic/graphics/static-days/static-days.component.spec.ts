import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticDaysComponent } from './static-days.component';

describe('StaticDaysComponent', () => {
  let component: StaticDaysComponent;
  let fixture: ComponentFixture<StaticDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
