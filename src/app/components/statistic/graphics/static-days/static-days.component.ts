import {Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';

declare var require: any;
const Highcharts = require('highcharts/highcharts.src');
import 'highcharts/adapters/standalone-framework.src';
import {NetflowDataService} from "../../../../services/netflow-data.service";
import {StatisticService} from "../../../../services/statistic.service";

@Component({
    selector: 'static-days-graph',
    templateUrl: './static-days.component.html',
    styleUrls: ['./static-days.component.css'],
})
export class StaticDaysComponent implements OnInit {


    @Input()
    days: string;

    @ViewChild('chart') public chartEl: ElementRef;
    private _chart: any;

    constructor(private statisticService: StatisticService) {

    }

    ngOnInit() {

        const ipSource = this.statisticService.getIpSource();
        const ipDestination = this.statisticService.getIpDestination();
        const days = "1";
        this.statisticService.getStaticData(ipSource, ipDestination, this.days)
            .subscribe(instance => {
                this._chart.addSeries({

                    data: (function () {
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        var n = +days;

                        for (i = -23; i <= 0; i += 1) {
                            data.push({
                                x: time + i * n * 1000 * 3600,
                                y: instance[23 + i]
                            });
                        }
                        return data;
                    }())

                })
            })
    }


    public ngAfterViewInit() {

        let opts: any = {
            title: {
                text: 'Utilization of link'
            },
            yAxis: {
                max: 100,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{
                    from: 85,
                    to: 85,
                    color: "rgba(255,0,0,0.30)"
                },
                    {
                        from: 50,
                        to: 50,
                        color: "rgba(0,255,0,0.30)"
                    }]
            },
            chart: {

                events: {
                    addSeries: function () {
                        var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                            .attr({
                                fill: Highcharts.getOptions().colors[0],
                                padding: 10,
                                r: 5,
                                zIndex: 8
                            })
                            .css({
                                color: '#FFFFFF'
                            })
                            .add();

                        setTimeout(function () {
                            label.fadeOut();
                        }, 1000);
                    }
                }

            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },

        };

        if (this.chartEl && this.chartEl.nativeElement) {
            opts.chart = {
                type: 'spline',
                backgroundColor: "#FAFAFA",
                renderTo: this.chartEl.nativeElement
            };

            this._chart = new Highcharts.Chart(opts);

        }
    }

    public ngOnDestroy() {
        this._chart.destroy();
    }
}
