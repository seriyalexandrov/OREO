import {Component, OnInit} from '@angular/core';
import {StatisticService} from "../../services/statistic.service";
import {Router} from "@angular/router";
import {Subject} from "rxjs";
import {NotificationsService, Notification} from "../../services/notifications.service";

@Component({
    selector: 'app-statistic',
    templateUrl: './statistic.component.html',
    styleUrls: ['./statistic.component.css'],
})
export class StatisticComponent implements OnInit {

    router: Router;
    destroy$: Subject<any> = new Subject();
    notifications: Notification[] = [];

    localDateFrom: Date;          //variablse used in popup modal
    localDateTo: Date;
    localPointsPerDay: string;

    dateFrom: Date;
    dateTo: Date;
    pointsPerDay: string;
    showModal: boolean;
    isReady: boolean;
    opened: boolean = true;

    constructor(private statisticService: StatisticService, _router: Router, private notificationService: NotificationsService) {
        this.showModal = true;
        this.isReady = false;
        this.router = _router;
    }

    ngOnInit() {
        this.notifications = this.notificationService.getNotifications();
        this.notificationService.notificationsUpdate$.takeUntil(this.destroy$).subscribe((event) => {
            this.notifications = this.notificationService.getNotifications();
        })
    }

    cancelModal() {
        if(this.dateFrom && this.dateTo && this.pointsPerDay){
            this.showModal = false;
        }
        else {
            this.isReady = false;
            this.showModal = false;
        }
    }
    okModal() {
        if(this.localDateFrom && this.localDateTo && this.localPointsPerDay){
            if(this.dateFrom === this.localDateFrom && this.dateTo === this.localDateTo && this.pointsPerDay === this.localPointsPerDay){
                this.showModal = false;
            }
            else {
                this.dateTo = this.localDateTo;
                this.dateFrom = this.localDateFrom;
                this.pointsPerDay = this.localPointsPerDay;

                this.statisticService.setDateFrom(this.dateFrom);
                this.statisticService.setDateTo(this.dateTo);
                this.statisticService.setCustomNumOfPoints(this.pointsPerDay)

                this.statisticService.customMessages$.next({"data" : {}});
                this.isReady = true;
                this.showModal = false;
            }
        }
        else {
            this.isReady = false;
            this.showModal = false;
        }
    }
    goToSettings(){
        this.router.navigateByUrl('/settings');

    }
    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
