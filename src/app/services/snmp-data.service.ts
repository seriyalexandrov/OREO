import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';

export class SnmpData {
    snmpData: string;
}


@Injectable()
export class SnmpDataService {

    constructor(private http: Http) {
    }

    getSnmpData(): Observable<SnmpData> {
        return this.http.get(`/rest/update/snmp`)
            .map(res => res.json())
    }

}
