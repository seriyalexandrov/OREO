import { TestBed, inject } from '@angular/core/testing';

import { SnmpDataService } from './snmp-data.service';

describe('SnmpDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SnmpDataService]
    });
  });

  it('should be created', inject([SnmpDataService], (service: SnmpDataService) => {
    expect(service).toBeTruthy();
  }));
});
