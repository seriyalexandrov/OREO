import {TestBed, inject} from '@angular/core/testing';

import {NetflowDataService} from './netflow-data.service';

describe('NetflowDataService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [NetflowDataService]
        });
    });

    it('should ...', inject([NetflowDataService], (service: NetflowDataService) => {
        expect(service).toBeTruthy();
    }));
});
