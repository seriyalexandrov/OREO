import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {Http} from "@angular/http";

@Injectable()
export class SseService {

    subscribe(): Subject<any> {
        let eventSource = new EventSource(`/rest/sniffState/subscribe`);

        let subscription = new Subject();
        eventSource.addEventListener("message", event => {
            subscription.next(event);
        });
        return subscription;
    }

    constructor(private http: Http) {
    }


}
