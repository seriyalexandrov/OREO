import {Injectable} from '@angular/core'

import {Http} from '@angular/http'
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';


export class Sniffer {
    id: string;
    status: string;
    port: string;

}

@Injectable()
export class SnifferStateService {

    constructor(private http: Http) {
    }

    getSnifferInstance(): Observable<Sniffer> {
        return this.http.get(`/rest/sniffState`)
            .map(res => res.json())
    }

    runSniffer(port: String):  Observable<Sniffer> {
        return this.http.get(`/rest/sniffState/doRun?port=${port}`)
            .map(res => res.json())
    }

    stopSniffer(port: String):  Observable<Sniffer> {
        return this.http.get(`/rest/sniffState/doStop?port=${port}`)
            .map(res => res.json())
    }
    log(): Observable<boolean> {
        return this.http.get(`/rest/checkLog`)
            .map(res => res.json())
    }
}


