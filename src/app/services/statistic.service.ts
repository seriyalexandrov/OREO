import {Injectable} from '@angular/core';
import {Observable, Subject} from "rxjs";
import {Http} from "@angular/http";
import {Link} from "./links.service";

@Injectable()
export class StatisticService {

    selectedLink: Link;

    messages$: Subject<any> = new Subject();

    customMessages$: Subject<any> = new Subject();


    dateFrom: Date;
    dateTo: Date;
    customNumOfPints: string;

    getCustomNumOfPoints(): string {
        return this.customNumOfPints;
    }

    getDateTo(): Date {
        return this.dateTo;

    }

    getDateFrom(): Date {
        return this.dateFrom;
    }

    setCustomNumOfPoints(numOfPoints: string){
        this.customNumOfPints = numOfPoints;
    }

    setDateTo(dateTo: Date){
        this.dateTo = dateTo;
    }

    setDateFrom(dateFrom: Date){
       this.dateFrom = dateFrom;
    }

    getIpSource(): string {
        return this.selectedLink.source;
    }

    getIpDestination(): string {
        return this.selectedLink.destination;
    }

    setSelectedLink(link: Link) {
        this.selectedLink = link;
    }
    getSelectedLink(): Link {
        return this.selectedLink;
    }

    constructor(private http: Http) {

    }

    getInitData(ipSource: string, ipDestination: string, numOfPoints: string): Observable<number[]> {
        return this.http.get(
            `/rest/link/current_stat/init?ipSource=${ipSource}&ipDestination=${ipDestination}&numOfPoints=${numOfPoints}`
        ).map(res => res.json())
    }

    getCurrentUtilization(ipSource: string, ipDestination: string): Observable<number> {
        return this.http.get(
            `/rest/link/current_stat?ipSource=${ipSource}&ipDestination=${ipDestination}`
        ).map(res => res.json())
    }

    getStaticData(ipSource: string, ipDestination: string, days: string): Observable<number[]> {
        return this.http.get(
            `/rest/link/list/days?ipSource=${ipSource}&ipDestination=${ipDestination}&numOfDays=${days}`
        ).map(res => res.json())
    }


    getCustomStaticData(ipSource: string, ipDestination: string, dateFrom: string, dateTo: string, numOfPoints: string): Observable<number[]> {
        return this.http.get(
            `/rest/link/list/customDays?ipSource=${ipSource}&ipDestination=${ipDestination}&dateFrom=${dateFrom}&dateTo=${dateTo}&numOfPoints=${numOfPoints}`
        ).map(res => res.json())
    }




}
