import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {SecurityComponent} from '../components/security/security.component';

@Injectable()
export class SecurityService {

constructor(private http: Http) {
    }

    sendUser(login: String, password: String):  Observable<Boolean> {
        return this.http.get(`/rest/registrate/doLogin?login=${login}&password=${password}`)
            .map(res => res.json())
    }

}
