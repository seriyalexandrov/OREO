import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable, Subject} from "rxjs";

export class IPsource {
    id: string;
    ip: string;
    port: string;
}


@Injectable()
export class NetflowDataService {

    constructor(private http: Http) {
    }

    selectedRegime: string;
    selectedIP: string;

    setSelectedRegime(regime: string) {
        this.selectedRegime = regime;
    }

    getSelectedRegime(): string {
        return this.selectedRegime;
    }

    setSelectedIp(ip: string) {
        this.selectedIP = ip;
    }

    getSelectedIp(): string {
        return this.selectedIP;
    }

    messages$: Subject<any> = new Subject();


    getIpSources(): Observable<IPsource[]> {
        return this.http.get(`/rest/interface/list`)
            .map(res => res.json())
    }

    getInitData(selectedIp: string, numOfPoints: string): Observable<number[]> {
        return this.http.get(`/rest/interface/current_stat/init?ipSource=${selectedIp}&numOfPoints=${numOfPoints}`)
            .map(res => res.json())
    }

    getCurrentTraffic(selectedIp: string): Observable<number> {
        return this.http.get(`/rest/interface/current_stat?ipSource=${selectedIp}`)
            .map(res => res.json())
    }

    getStaticDataOfDays(selectedIp: string, numOfDays: string): Observable<number[]> {
        return this.http.get(`/rest/interface/list/days?ipSource=${selectedIp}&numOfDays=${numOfDays}`)
            .map(res => res.json())
    }
}
