import {Injectable} from '@angular/core';
import {Subject} from "rxjs";

export class Notification{
    date: Date;
    type: string;

    constructor(date: Date, type: string) {
        this.date = date;
        this.type = type;
    }
}

@Injectable()
export class NotificationsService {


    notificationsUpdate$: Subject<any> = new Subject();
    notifications: Notification[] = [];


    constructor() {
    }


    addNotification(type: string) {
        this.notifications.push(new Notification((new Date()), type));
        this.notificationsUpdate$.next({"data" : {}})
    }

    getNotifications(): Notification[] {
        return this.notifications;
    }

}
