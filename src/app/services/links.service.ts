import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Http} from "@angular/http";
import {Comparator} from "clarity-angular";

export class Link {
    id: string;
    name: string;
    source: string;
    destination: string;
    utilization: string;
}
export class LinkComparator implements Comparator<Link> {
    compare(a: Link, b: Link) {
        return  parseInt(b.utilization) - parseInt(a.utilization);
    }
}


@Injectable()
export class LinksService {

    constructor(private http: Http) {
    }

    getLinks(): Observable<Link[]> {
        return this.http.get(`/rest/link/list`)
            .map(res => res.json())
    }


    updateLink(id: string, newName: String): Observable<Link> {
        return this.http.get(`/rest/link/edit?id=${id}&name=${newName}`)
            .map(res => res.json())
    }
}

