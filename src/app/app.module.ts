import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';

import {RouterModule, Routes} from '@angular/router';
import {SnifferStateComponent} from "./components/net-flow/snifferState/snifferState.component";
import {AppComponent} from "./app.component"

import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {ClarityModule} from "clarity-angular";
import {NetFlowComponent} from './components/net-flow/net-flow.component';
import {NetFlowDescriptionComponent} from './components/net-flow/net-flow-description/net-flow-description.component';
import {ChartModule} from 'angular-highcharts';
import {MainPageComponent} from './components/main-page/main-page.component';
import {SettingsComponent} from './components/settings/settings.component';
import { PopUpComponent } from './components/settings/pop-up/pop-up.component';
import {NetflowDatagridComponent} from './components/net-flow/netflow-datagrid/netflow-datagrid.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RealtimeStatComponent} from './components/net-flow/netflow-datagrid/realtime-stat/realtime-stat.component';
import {NetflowDataService} from "./services/netflow-data.service";
import {DaysStatComponent} from './components/net-flow/netflow-datagrid/days-stat/days-stat.component';
import {SseService} from "./services/sse.service";
import {StatisticService} from "./services/statistic.service";
import {RealtimeComponent} from './components/statistic/graphics/realtime/realtime.component';
import { StatisticComponent } from './components/statistic/statistic.component';
import { CustomComponent } from './components/statistic/graphics/custom/custom.component';
import {StaticDaysComponent} from "./components/statistic/graphics/static-days/static-days.component";
import {LinksService} from "./services/links.service";
import {NotificationsService} from "./services/notifications.service";
import {SecurityComponent} from './components/security/security.component';
import {SecurityService} from "./services/security.service";
import {SnifferStateService} from "./services/snifferState.service";
const appRoutes: Routes = [

    {path: '', component: MainPageComponent, pathMatch: 'full'},
    {
        path: 'netflow', component: NetFlowComponent, children: [
        {path: '', redirectTo: 'description', pathMatch: 'full'},
        {path: 'status', component: SnifferStateComponent},
        {path: 'description', component: NetFlowDescriptionComponent},
        {path: 'info', component: NetflowDatagridComponent}

    ]
    },
    {path: 'statistics', component: StatisticComponent},

    {path: 'registration', component: SecurityComponent},
    {path: 'settings', component: SettingsComponent}
];

@NgModule({
    declarations: [
        SnifferStateComponent,
        AppComponent,
        NetFlowComponent,
        NetFlowDescriptionComponent,
        MainPageComponent,
        SettingsComponent,
        PopUpComponent,
        NetflowDatagridComponent,
        RealtimeStatComponent,
        DaysStatComponent,
        RealtimeComponent,
        StatisticComponent,
        CustomComponent,
        StaticDaysComponent,
        SecurityComponent,
    ],
    imports: [
        NguiAutoCompleteModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        ClarityModule,
        ChartModule,
        BrowserAnimationsModule
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        NetflowDataService,
        SseService,
        SecurityService,
        StatisticService,
        LinksService,
        NotificationsService,
        SnifferStateService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
